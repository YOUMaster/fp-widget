import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { WidgetContainerService } from '../widget-container/widget-container.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'fp-payment-form',
  templateUrl: './payment-form.component.html',
  styleUrls: ['./payment-form.component.less']
})
export class PaymentFormComponent implements OnInit, OnDestroy {
  public amount: number;
  public isSubmited: boolean = false;
  public form: FormGroup;

  private _subscriptions: Subscription[] = [];

  constructor(
    private _widgetContainerService: WidgetContainerService
  ) { }

  ngOnInit() {
    this.form = new FormGroup({
      cardholderName: new FormControl('', [Validators.required, this.cardholderNameValidator]),
      cardNumber: new FormControl('', [Validators.required, this.cardNumberValidator]),
      expDate: new FormGroup({
        expYear: new FormControl('', [Validators.required, this.onlyNumbersValidator, this.yearValidator]),
        expMonth: new FormControl('', [Validators.required, this.onlyNumbersValidator, this.monthValidator]),
      }, this.expDateValidator),
      cvv: new FormControl('', [Validators.required, this.onlyNumbersValidator])
    });

    this._subscriptions = [
      this._widgetContainerService.amount$.subscribe(amount => {
        this.amount = amount;
      })
    ];
  }

  public ngOnDestroy() {
    if (this._subscriptions) {
      for (let subscription of this._subscriptions) {
        subscription.unsubscribe();
      }
      this._subscriptions = [];
    }
  }

  public onSubmit() {
    this.isSubmited = true;
  }

  public cardholderNameValidator(control: FormControl): any {
    const validate = /^[a-zA-Z]+$/; //only characters as indicated in the task
    let isValid = validate.test(control.value);

    if (!isValid) {
      return {
        'valueError': true
      };
    }

    return undefined;
  }

  public cardNumberValidator(control: FormControl): any {
    let arr = (control.value + '')
      .split('')
      .reverse()
      .map(x => parseInt(x));
    let lastDigit = arr.splice(0, 1)[0];
    let sum = arr.reduce((acc, val, i) => (i % 2 !== 0 ? acc + val : acc + ((val * 2) % 9) || 9), 0);
    sum += lastDigit;

    let isValid = sum % 10 === 0;
    if (!isValid) {
      return {
        'valueError': true
      };
    }

    return undefined;
  }

  public onlyNumbersValidator(control: FormControl): any {
    const validate = /^\d+$/;
    let isValid = validate.test(control.value);

    if (!isValid) {
      return {
        'valueError': true
      };
    }

    return undefined;
  }

  public monthValidator(control: FormControl): any {
    if (!(control.value > 0 && control.value < 13)) {
      return {
        'valueError': true
      };
    }

    return undefined;
  }

  public yearValidator(control: FormControl): any {
    const currenYear = new Date().getFullYear();
    if (!(control.value >= currenYear && control.value < 2100)) {
      return {
        'valueError': true
      };
    }

    return undefined;
  }

  public expDateValidator(formGroup: FormGroup): any {
    let today = new Date();
    let expDate = new Date();
    expDate.setFullYear(formGroup.value.expYear, formGroup.value.expMonth, 1);

    if (!(expDate >= today)) {
      return {
        'valueError': true
      };
    }

    return undefined;
  }
}
