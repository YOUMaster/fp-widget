import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError } from 'rxjs/internal/operators';
import { IPaymentMethod } from '../payment-methods/payment-methods.component';
@Injectable({
  providedIn: 'root'
})
export class ApiService {
  private _projectKey = '6af0c5844df890df97c9cc2f06d7a6c0';

  constructor(
    private _http: HttpClient
  ) { }

  public getPaymentSystems(countryCode: string): Observable<IPaymentMethod[]> {
    return this._http.get<IPaymentMethod[]>('https://api.paymentwall.com/api/payment-systems/?key=' + this._projectKey
      +'&country_code=' + countryCode).pipe(
        catchError(this._handleError<IPaymentMethod[]>('getPaymentSystems', [])));
  }

  public getGeolocationCountry(): Observable<any> {
    // return this._http.get<any>('https://api.paymentwall.com/api/rest/country?key=' + this._projectKey)
    //   .pipe(catchError(this._handleError<any>('getGeolocationCountry', [])));
    //The code above returns {type: "Error", object: "Error", error: "Internal error", code: null}

    //Alternative API
    return this._http.get<any>('https://ipinfo.io/json')
      .pipe(catchError(this._handleError<any>('getGeolocationCountry', [])));
  }

  private _handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      this._log(`${operation} failed: ${error.message}`);
      return of(result as T);
    };
  }

  private _log(message: string): void {
    alert("Something went wrong! Please try again later.");
    throw Error(message);
  }
}
