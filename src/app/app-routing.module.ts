import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { WidgetContainerComponent } from './widget-container/widget-container.component';


const routes: Routes = [
  {path: '', component: WidgetContainerComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
