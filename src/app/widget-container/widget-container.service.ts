import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class WidgetContainerService {
  private _minAmountValue: number = 1;

  public amount$: BehaviorSubject<number> = new BehaviorSubject<number>(this._minAmountValue);
  public get minAmountValue(): number {
    return this._minAmountValue;
  }
}
