import { Component, OnInit } from '@angular/core';
import { IPaymentMethod } from '../payment-methods/payment-methods.component';
import { Subscription } from 'rxjs';
import { ApiService } from '../shared/api.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { WidgetContainerService } from './widget-container.service';

@Component({
  selector: 'fp-widget-container',
  templateUrl: './widget-container.component.html',
  styleUrls: ['./widget-container.component.less']
})
export class WidgetContainerComponent implements OnInit {
  public form: FormGroup;
  public minAmountValue: number = this._widgetContainerService.minAmountValue;
  public amount: number = this.minAmountValue;
  public countryCode: string;
  public payments: IPaymentMethod[];

  private _subscriptions: Subscription[] = [];

  constructor(
    private _api: ApiService,
    private _widgetContainerService: WidgetContainerService
  ) {}

  public ngOnInit() {
    this._subscriptions = [
      this._api.getGeolocationCountry().subscribe((response: any) => {
        console.log(response);
        this.countryCode = response.country;
      }),
    ];

    this.form = new FormGroup({
      amount: new FormControl(
        this.minAmountValue,
        [Validators.required, this.onlyNumbersValidator, Validators.min(this.minAmountValue)]
      ),
    });
  }

  public ngOnDestroy() {
    if (this._subscriptions) {
      for (let subscription of this._subscriptions) {
        subscription.unsubscribe();
      }
      this._subscriptions = [];
    }
  }

  public updatePaymentMethods(countryCode: string): void {
    this._api.getPaymentSystems(countryCode).subscribe((data: IPaymentMethod[]) => {
      this.payments = data;
    })
  }

  public onlyNumbersValidator(control: FormControl): any {
    const validate = /^\d+$/;
    let isValid = validate.test(control.value);

    if (!isValid) {
      return {
        'valueError': true
      };
    }

    return undefined;
  }

  public onAmountChanged(): void {
    this._widgetContainerService.amount$.next(this.amount);
  }
}
