import { Component, OnInit, Input, ViewChild, ViewContainerRef, ComponentFactoryResolver, ComponentRef } from '@angular/core';
import { ApiService } from '../shared/api.service';
import { Subscription } from 'rxjs';
import { PaymentFormComponent } from '../payment-form/payment-form.component';

export interface IPaymentMethod {
  id: string;
  name: string;
  new_window: boolean;
  img_url: string;
  img_class: string;
  ps_type_id: number;
}

@Component({
  selector: 'fp-payment-methods',
  templateUrl: './payment-methods.component.html',
  styleUrls: ['./payment-methods.component.less']
})

export class PaymentMethodsComponent implements OnInit {
  @Input() public countryCode: string;
  @Input() public payments: IPaymentMethod[];
  @ViewChild('formContainer', {static: false, read: ViewContainerRef}) formContainer: ViewContainerRef;

  private _subscriptions: Subscription[] = [];
  private _componentRef: ComponentRef<any>;

  constructor(
    private _api: ApiService,
    private _resolver: ComponentFactoryResolver
  ) {}

  ngOnInit() {
    this._subscriptions = [
      this._api.getPaymentSystems(this.countryCode).subscribe((data: IPaymentMethod[]) => {
        this.payments = data;
      })
    ];
  }

  public ngOnDestroy() {
    if (this._subscriptions) {
      for (let subscription of this._subscriptions) {
        subscription.unsubscribe();
      }
      this._subscriptions = [];
    }

    if (this._componentRef) {
      this._componentRef.destroy();
    }
  }

  public onPaymentSelect(): void {
    this.formContainer.clear();
    const innerFormFactory = this._resolver.resolveComponentFactory(PaymentFormComponent);
    this._componentRef = this.formContainer.createComponent(innerFormFactory);
  }
}
