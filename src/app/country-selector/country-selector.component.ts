import { Component, OnInit, OnDestroy, Output, EventEmitter, Input } from '@angular/core';
import { Subscription } from 'rxjs';
import { CountrySelectorService } from './country-selector.service';

export interface ICountry {
  code: string,
  name: string
}

@Component({
  selector: 'fp-country-selector',
  templateUrl: './country-selector.component.html',
  styleUrls: ['./country-selector.component.less']
})
export class CountrySelectorComponent implements OnInit, OnDestroy {
  @Input() public countryCode: string;
  @Output() onSelectCountry: EventEmitter<string> = new EventEmitter<string>();

  public countries: ICountry[];

  private _subscriptions: Subscription[] = [];

  constructor(
    private _countrySelectorService: CountrySelectorService
  ) {}

  public ngOnInit() {
    this.countries = this._countrySelectorService.countryList;
  }

  public ngOnDestroy() {
    if (this._subscriptions) {
      for (let subscription of this._subscriptions) {
        subscription.unsubscribe();
      }
      this._subscriptions = [];
    }
  }

  public selectCountry(country: string): void {
    this.countryCode = country;
    this.onSelectCountry.next(this.countryCode);
  }
}
